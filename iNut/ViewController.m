//
//  ViewController.m
//  iNut
//
//  Created by IT-Högskolan on 18/02/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "ViewController.h"
#import "MechanicsViewController.h"
#import "UserDataViewController.h"
#import "LDFProduktInformation.h"
#import "SearchViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIButton *infoButton;
@property (weak, nonatomic) IBOutlet UIButton *showButton;
@property (weak, nonatomic) IBOutlet UIButton *myPageButton;

@property (nonatomic) NSMutableArray *userArray;
@end

@implementation ViewController


- (void)viewDidLoad{
    [super viewDidLoad];
    self.productArray = [[NSMutableArray alloc] init];
    self.userArray = [[NSMutableArray alloc] init];
    self.navigationItem.hidesBackButton = YES;
    [MechanicsViewController setButtonGraphics:self.showButton];
    [MechanicsViewController setButtonGraphics:self.myPageButton];
    [MechanicsViewController setButtonGraphics:self.infoButton];
    self.userArray = [MechanicsViewController loadUserData];
    self.productArray = [MechanicsViewController searchMatApi];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"toTableView"]){
        SearchViewController *addView = [ segue destinationViewController ];
        addView.productArray = self.productArray;
        addView.userArray = self.userArray;
    } else if ([segue.identifier isEqualToString:@"toUSerProd"]){
        UserDataViewController *addView = [ segue destinationViewController ];
        addView.userArray = self.userArray;
    }
}

@end
