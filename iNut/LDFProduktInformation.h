//
//  LDFProduktInformation.h
//  iNut
//
//  Created by IT-Högskolan on 22/02/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LDFProduktInformation : NSObject <NSCoding>
@property (nonatomic) NSString *produktName;
@property (nonatomic) NSString *produktIndexNr;
@property (nonatomic) NSString *fatIndex;
@property (nonatomic) NSString *energyIndex;
@property (nonatomic) NSString *waterIndex;
@property (nonatomic) NSString *proteinIndex;
@property (nonatomic) NSString *imagePath;
@property (nonatomic) BOOL expandView;
@property (nonatomic) BOOL isShown;
@property (nonatomic) BOOL isFav;
@property (nonatomic) BOOL toCompare;
@end
