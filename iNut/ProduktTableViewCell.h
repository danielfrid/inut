//
//  ProduktTableViewCell.h
//  iNut
//
//  Created by IT-Högskolan on 22/02/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProduktTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *produktLabel;
@property (weak, nonatomic) IBOutlet UILabel *proteinLabel;
@property (weak, nonatomic) IBOutlet UILabel *fettLabel;
@property (weak, nonatomic) IBOutlet UILabel *energyLabel;
@property (weak, nonatomic) IBOutlet UILabel *waterLabel;
@property (weak, nonatomic) IBOutlet UILabel *watLabel;
@property (weak, nonatomic) IBOutlet UILabel *nrjLabel;
@property (weak, nonatomic) IBOutlet UILabel *proLabel;
@property (weak, nonatomic) IBOutlet UILabel *fatLabel;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UIImageView *blurAddImg;


@property (nonatomic) NSArray *uiLabels;

@end
