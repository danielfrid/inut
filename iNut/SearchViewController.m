//
//  SearchViewController.m
//  iNut
//
//  Created by IT-Högskolan on 20/02/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "SearchViewController.h"
#import "MechanicsViewController.h"
#import "UserDataViewController.h"
#import "ProduktTableViewCell.h"
#import "LDFProduktInformation.h"

@interface SearchViewController ()
@property (nonatomic) BOOL isFilltered;
@property (weak, nonatomic) IBOutlet UIButton *myButton;

@property (nonatomic) NSMutableArray *filteredArray;
@property (nonatomic) LDFProduktInformation *thisProd;
@property (nonatomic) ProduktTableViewCell *cloneCell;
@end

@implementation SearchViewController
- (void)viewDidLoad{
    [super viewDidLoad];
    [MechanicsViewController setButtonGraphics:self.myButton];
    
    //sätter expandView NO för att undvika problem med öppna celler om man återgår till main och sedan
    //tillbaks till tableView
    for(LDFProduktInformation *p in self.productArray){
        p.expandView = NO;
    }
}


-(void)viewWillLayoutSubviews{
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)addButton:(UIButton*)sender {
   
    CGPoint buttonPosition = [sender convertPoint:CGPointZero
                                           toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    NSLog(@"index.row in addButton: %ld", (long)indexPath.row);
   
    if(self.isFilltered){
        self.thisProd = self.filteredArray[indexPath.row];
    } else {
        self.thisProd = self.productArray[indexPath.row];
    }
    
    self.cloneCell = (ProduktTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath];
    
    if(!self.thisProd.isFav) {
        self.thisProd.isFav = YES;
        [self.userArray addObject:self.thisProd];
        [MechanicsViewController saveToFile:self.userArray];
        [self buttAnimation:YES];

    } else{
        self.thisProd.isFav = NO;
        for (int i = 0; i < self.userArray.count; i++) {
            LDFProduktInformation *check = self.userArray[i];
            if (check.produktIndexNr == self.thisProd.produktIndexNr) {
                [self.userArray removeObjectAtIndex:i];
                break;
            }
        }
        
        [MechanicsViewController saveToFile:self.userArray];
        [self buttAnimation:NO];

    }

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(self.isFilltered){
        return [self.filteredArray count];
    }
    return [self.productArray count];
    }


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ProduktTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    LDFProduktInformation *prod;
    
    if(!self.isFilltered){
        prod = [self.productArray objectAtIndex:indexPath.row];
    }
    else {
        prod = [self.filteredArray objectAtIndex:indexPath.row];

    }
    cell.produktLabel.text = prod.produktName;
    [MechanicsViewController setFavButtGraphics:cell.addButton];
    if(prod.isFav){
        [cell.addButton setTitle:@"del" forState:UIControlStateNormal];
    } else {
        [cell.addButton setTitle:@"add" forState:UIControlStateNormal];
    }
    for (UILabel *label in cell.uiLabels) {
        label.hidden = !prod.expandView;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.isFilltered){
        self.thisProd = self.filteredArray[indexPath.row];
    } else {
    self.thisProd = self.productArray[indexPath.row];
    }
    
    for(LDFProduktInformation *favprod in self.userArray){
        if(favprod.produktIndexNr == self.thisProd.produktIndexNr){
            self.thisProd.isFav = YES; break;
        }
    }
    self.thisProd.expandView = !self.thisProd.expandView;
    self.thisProd.isShown = !self.thisProd.isShown;
   
    ProduktTableViewCell *cell = (ProduktTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    self.cloneCell = cell;
    [self loadApiDetails:self.thisProd.produktIndexNr forCell:cell andTableView:self.tableView withProd:self.thisProd];
   }

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    LDFProduktInformation *thisProd;
    if(self.isFilltered){
    thisProd = self.filteredArray[indexPath.row];
    } else {
    thisProd = self.productArray[indexPath.row];
    }
    int height = 30;
    if(thisProd.expandView){
        height = 70;
    }
    return height;
}


- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"toUserProd"]){
        UserDataViewController *user = [ segue destinationViewController ];
        user.userArray = self.userArray;
    } 
}


#pragma mark - animations
- (void) buttAnimation:(BOOL)add {
    
  
//    CGPoint startPos = self.cloneCell.blurAddImg.center;

    [UIView animateWithDuration:0.7 animations:^{
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        if(add){
            self.cloneCell.blurAddImg.hidden = NO;
            //self.cloneCell.blurAddImg.center = CGPointMake(500, startPos.y);
            [self.cloneCell.addButton setTitle: @"del" forState:UIControlStateNormal];
            
        } else{
            self.cloneCell.blurAddImg.hidden = NO;
        //    self.cloneCell.blurAddImg.center = CGPointMake(-300, startPos.y);
            [self.cloneCell.addButton setTitle: @"add" forState:UIControlStateNormal];
        }
        [self.cloneCell.blurAddImg setAlpha:0.0f];
    } completion:^(BOOL finished) {
        self.cloneCell.blurAddImg.hidden = YES;
     //   self.cloneCell.blurAddImg.center = startPos;
        [UIView animateWithDuration:0 animations:^{
            [UIView setAnimationDuration:0.001f];
        } completion:^(BOOL finished) {
            [UIView setAnimationDuration:0];
            [self.cloneCell.blurAddImg setAlpha:1.0f];
            [self.tableView reloadData];
        }];
    }];
}

#pragma mark - SearchBar

- (void) searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self.tableView resignFirstResponder];
    }

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if(searchText.length == 0){
        self.isFilltered = NO;
    } else {

        self.isFilltered = YES;
        self.filteredArray = [[NSMutableArray alloc] init];
        for(LDFProduktInformation *prod in self.productArray)
        {   NSString *str = prod.produktName;
            NSRange stringRange = [str rangeOfString:searchText options:NSCaseInsensitiveSearch];
            if(stringRange.location != NSNotFound)
            {
                [self.filteredArray addObject:prod];
            }
        }
    }
    [self.tableView reloadData];
}


#pragma mark - API search

-(void)loadApiDetails:(NSString *)searchText forCell:(ProduktTableViewCell *)cell andTableView:(UITableView *)view withProd:(LDFProduktInformation *)prod{
    
    NSString *apiUrlNutri = [NSString stringWithFormat: @"http://www.matapi.se/foodstuff/%@", searchText];
    NSURL *url = [NSURL URLWithString:apiUrlNutri];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [ NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error) {return;}
        NSError *parsError = nil;
        NSDictionary *root2;
        root2 = [NSJSONSerialization JSONObjectWithData: data options:kNilOptions error:&parsError];
        if(!parsError){
            dispatch_async(dispatch_get_main_queue(), ^{
                if(root2.count > 0){
                    NSDictionary *tess = [root2 objectForKey:@"nutrientValues"];
                    NSString *fat = [NSString stringWithFormat:@"%@", [tess objectForKey:@"fat"]];
                    NSString *wat = [NSString stringWithFormat:@"%@", [[root2 objectForKey:@"nutrientValues"] objectForKey:@"water"]];
                    NSString *pro = [NSString stringWithFormat:@"%@", [[root2 objectForKey:@"nutrientValues"] objectForKey:@"protein"]];
                    NSString *nrj = [NSString stringWithFormat:@"%@", [[root2 objectForKey:@"nutrientValues"] objectForKey:@"energyKj"]];
                    cell.fettLabel.text = [NSString stringWithFormat:@"%@g",fat ];
                    cell.proteinLabel.text = [NSString stringWithFormat:@"%@g",pro];
                    cell.waterLabel.text = [NSString stringWithFormat:@"%@g",wat];
                    cell.energyLabel.text = [NSString stringWithFormat:@"%@kj",nrj];
                    prod.fatIndex = fat;
                    prod.proteinIndex = pro;
                    prod.waterIndex = wat;
                    prod.energyIndex = nrj;
                    
                    [view reloadData];
                }
            });
        }
    }];
    [task resume];
}


@end
