//
//  GraphicViewController.m
//  iNut
//
//  Created by IT-Högskolan on 20/02/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "MechanicsViewController.h"
#import "LDFProduktInformation.h"
#import "ProduktTableViewCell.h"

@interface MechanicsViewController ()
@end

@implementation MechanicsViewController

+(NSMutableArray *) searchMatApi{
    NSMutableArray *productArray = [[NSMutableArray alloc] init];
    NSString *apiUrl = [NSString stringWithFormat: @"http://www.matapi.se/foodstuff?query="];
    NSURL *url = [NSURL URLWithString:apiUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [ NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    if(error) {return;}
    
    NSError *parsError = nil;
    NSArray *root = [NSJSONSerialization JSONObjectWithData: data options:kNilOptions error:&parsError];
    
    if(!parsError){
        dispatch_async(dispatch_get_main_queue(), ^{
            if(root.count > 0 ){
                    for (int i = 0; i < root.count; i++) {
                        LDFProduktInformation *p1 =[[LDFProduktInformation alloc] init];
                        p1.produktName = root[i][@"name"];
                        p1.produktIndexNr = root[i][@"number"];
                        p1.expandView = NO;
                        p1.isShown = NO;
                        p1.isFav = NO;
                        [productArray addObject:p1];
                    }
            }
        });
        }
    }];
    [task resume];
    return productArray;
}

/*
    Couldn't use this method in Mechanics. Had to move it to the caller, SearchViewController, to avoid 
    problems with fetching data from API and write it to GUI.
*/
+(void)loadApiDetails:(NSString *)searchText forCell:(ProduktTableViewCell *)cell andTableView:(UITableView *)view{
    
    NSString *apiUrlNutri = [NSString stringWithFormat: @"http://www.matapi.se/foodstuff/%@", searchText];
    NSURL *url = [NSURL URLWithString:apiUrlNutri];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [ NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error) {return;}
        NSError *parsError = nil;
        NSDictionary *root2;
        root2 = [NSJSONSerialization JSONObjectWithData: data options:kNilOptions error:&parsError];
        if(!parsError){
            dispatch_async(dispatch_get_main_queue(), ^{
                if(root2.count > 0){
                    NSDictionary *tess = [root2 objectForKey:@"nutrientValues"];
                    NSString *fat = [NSString stringWithFormat:@"%@", [tess objectForKey:@"fat"]];
                    NSString *wat = [NSString stringWithFormat:@"%@", [[root2 objectForKey:@"nutrientValues"] objectForKey:@"water"]];
                    NSString *pro = [NSString stringWithFormat:@"%@", [[root2 objectForKey:@"nutrientValues"] objectForKey:@"protein"]];
                    NSString *nrj = [NSString stringWithFormat:@"%@", [[root2 objectForKey:@"nutrientValues"] objectForKey:@"energyKj"]];
                    cell.fettLabel.text = [NSString stringWithFormat:@"%@g",fat ];
                    cell.proteinLabel.text = [NSString stringWithFormat:@"%@g",pro];
                    cell.waterLabel.text = [NSString stringWithFormat:@"%@g",wat];
                    cell.energyLabel.text = [NSString stringWithFormat:@"%@kj",nrj];
                   [view reloadData];
                }
            });
        }
    }];
    [task resume];
}



+(void)setButtonGraphics:(UIButton *)btn{
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor yellowColor] forState:UIControlStateHighlighted];
    
    CAGradientLayer *btnGradient = [CAGradientLayer layer];
    btnGradient.frame = btn.bounds;
    btnGradient.colors = [NSArray arrayWithObjects:
                          (id)[[UIColor colorWithRed:58.0f / 255.0f green:83.0f / 255.0f blue:155.0f / 255.0f alpha:1.0f] CGColor],
                          (id)[[UIColor colorWithRed:51.0f / 255.0f green:51.0f / 255.0f blue:51.0f / 255.0f alpha:1.0f] CGColor],
                          nil];
    [btn.layer insertSublayer:btnGradient atIndex:0];
    
    CALayer *btnLayer = [btn layer];
    [btnLayer setMasksToBounds:YES];
    [btnLayer setCornerRadius:5.0f];
    [btnLayer setBorderWidth	:1.0f];
    [btnLayer setBorderColor:[[UIColor blackColor] CGColor]];
}


+(void)setAddButtonGraphics:(UIButton *)btn{
    
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor yellowColor] forState:UIControlStateHighlighted];
    
    CAGradientLayer *btnGradient = [CAGradientLayer layer];
    btnGradient.frame = btn.bounds;
    btnGradient.colors = [NSArray arrayWithObjects:
                          (id)[[UIColor colorWithRed:137.0f / 255.0f green:189.0f / 255.0f blue:46.0f / 255.0f alpha:1.0f] CGColor],
                          (id)[[UIColor colorWithRed:32.0f / 255.0f green:82.0f / 255.0f blue:12.0f / 255.0f alpha:1.0f] CGColor],
                          nil];
    [btn.layer insertSublayer:btnGradient atIndex:0];
    
    CALayer *btnLayer = [btn layer];
    [btnLayer setMasksToBounds:YES];
    [btnLayer setCornerRadius:5.0f];
    [btnLayer setBorderWidth	:1.0f];
    [btnLayer setBorderColor:[[UIColor grayColor] CGColor]];
}


+(void)setRemButtonGraphics:(UIButton *)btn{
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor yellowColor] forState:UIControlStateHighlighted];
    
    CAGradientLayer *btnGradient = [CAGradientLayer layer];
    btnGradient.frame = btn.bounds;
    btnGradient.colors = [NSArray arrayWithObjects:
                          (id)[[UIColor colorWithRed:229.0f / 255.0f green:32.0f / 255.0f blue:32.0f / 255.0f alpha:1.0f] CGColor],
                          (id)[[UIColor colorWithRed:99.0f / 255.0f green:21.0f / 255.0f blue:21.0f / 255.0f alpha:1.0f] CGColor],
                          nil];
    [btn.layer insertSublayer:btnGradient atIndex:0];
    
    CALayer *btnLayer = [btn layer];
    [btnLayer setMasksToBounds:YES];
    [btnLayer setCornerRadius:5.0f];
    [btnLayer setBorderWidth	:1.0f];
    [btnLayer setBorderColor:[[UIColor grayColor] CGColor]];
}


+(void)setCompareBoxGraphics:(UIView *)box{
    
    CAGradientLayer *boxGradient = [CAGradientLayer layer];
    boxGradient.frame = box.bounds;
    boxGradient.colors = [NSArray arrayWithObjects:
                          (id)[[UIColor colorWithRed:137.0f / 255.0f green:109.0f / 255.0f blue:90.0f / 255.0f alpha:1.0f] CGColor],
                          (id)[[UIColor colorWithRed:32.0f / 255.0f green:82.0f / 255.0f blue:90.0f / 255.0f alpha:1.0f] CGColor],
                          nil];
    [box.layer insertSublayer:boxGradient atIndex:0];
    
    CALayer *boxLayer = [box layer];
    [boxLayer setMasksToBounds:YES];
    [boxLayer setCornerRadius:5.0f];
    [boxLayer setBorderWidth	:1.0f];
    [boxLayer setBorderColor:[[UIColor grayColor] CGColor]];
}


+(void)setHollowGraphics:(UIView *)box{
  
    box.backgroundColor = [UIColor whiteColor];
    CALayer *boxLayer = [box layer];
    [boxLayer setMasksToBounds:YES];
    [boxLayer setCornerRadius:5.0f];
    [boxLayer setBorderWidth	:1.0f];
    [boxLayer setBorderColor:[[UIColor grayColor] CGColor]];
}
+(void)setFavButtGraphics:(UIView *)box{
    
    CAGradientLayer *boxGradient = [CAGradientLayer layer];
    boxGradient.frame = box.bounds;
    boxGradient.colors = [NSArray arrayWithObjects:
                          (id)[[UIColor colorWithRed:89.0f / 255.0f green:88.0f / 255.0f blue:243.0f / 255.0f alpha:1.0f] CGColor],
                          (id)[[UIColor colorWithRed:24.0f / 255.0f green:12.0f / 255.0f blue:82.0f / 255.0f alpha:1.0f] CGColor],
                          nil];
    [box.layer insertSublayer:boxGradient atIndex:0];
    
    CALayer *boxLayer = [box layer];
    [boxLayer setMasksToBounds:YES];
    [boxLayer setCornerRadius:10.0f];
    [boxLayer setBorderWidth	:1.0f];
    [boxLayer setBorderColor:[[UIColor grayColor] CGColor]];
}

+(void)saveToFile:(NSMutableArray *)userArray{
    NSData *encodedFood = [NSKeyedArchiver archivedDataWithRootObject:userArray];
    [[NSUserDefaults standardUserDefaults] setObject:encodedFood forKey:[NSString stringWithFormat:@"nutrients"]];
    NSLog(@"Done saving");
}

+(NSMutableArray *)loadUserData{
    NSData *decodedFood = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"nutrients"]];
    NSArray *decodedArray = [NSKeyedUnarchiver unarchiveObjectWithData:decodedFood];
        NSLog(@"Done Loading");
    return [NSMutableArray arrayWithArray:decodedArray];
}


@end
