//
//  LoadingScreen.m
//  iNut
//
//  Created by IT-Högskolan on 13/03/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "LoadingScreen.h"
#import "ViewController.h"
#import "MechanicsViewController.h"
@interface LoadingScreen()
@property (weak, nonatomic) IBOutlet UIView *container;
@property (weak, nonatomic) IBOutlet UIImageView *bottle;


@property (nonatomic) UIDynamicAnimator *anim;
@property (nonatomic) UIGravityBehavior *gravity;
@property (nonatomic) UICollisionBehavior *collision;
@property (nonatomic) NSMutableArray *parts;
@property (nonatomic) UIView *part; //tabort
@property (nonatomic) UIImageView *ball;
@property (nonatomic) NSTimer *timer;
@property (nonatomic) int times;
@property (nonatomic) NSMutableArray *productArray;
@end
@implementation LoadingScreen

- (void)viewDidLoad{
    [super viewDidLoad];
    self.productArray = [MechanicsViewController searchMatApi];
    self.parts = [[NSMutableArray alloc] init];
    self.anim = [[UIDynamicAnimator alloc] initWithReferenceView:self.container];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(startAnim) userInfo:nil repeats:NO];
    self.times = 0;
    
}
- (void) startAnim{

//    CGFloat ballY = self.bottle.frame.origin.y+(arc4random() %6);
//    self.part = [[UIView alloc] initWithFrame:CGRectMake(0,0,22,22)];
//    self.part.layer.cornerRadius = 11;
//    self.part.backgroundColor = [UIColor brownColor];
//    [self.parts addObject:self.part];
//   self.part.center = CGPointMake(ballY, 0);
    
    [self.container addSubview:self.part];
    
    self.gravity = [[UIGravityBehavior alloc] initWithItems:@[]];
    self.gravity.magnitude = 1;
    [self.anim addBehavior:self.gravity];

    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(rollBalls) userInfo:nil repeats:YES];

    self.collision = [[UICollisionBehavior alloc] initWithItems:@[]];
    self.collision.translatesReferenceBoundsIntoBoundary = YES;
    [self.anim addBehavior:self.collision];
  
}
- (void) rollBalls{
    self.times ++ ;
    self.part = [[UIView alloc] initWithFrame:CGRectMake(0,0,22,22)];
    CGFloat ballY = self.bottle.frame.origin.y+(arc4random() %20)-10;
    self.part.layer.cornerRadius = 11;
    self.part.backgroundColor = [UIColor brownColor];
   self.part.center = CGPointMake(ballY, 20);
    [self.parts addObject:self.part];

    [self.container addSubview:self.part];
    [self.gravity addItem:self.part];
    [self.anim addBehavior:self.gravity];
    self.collision = [[UICollisionBehavior alloc] initWithItems:self.parts];
    self.collision.translatesReferenceBoundsIntoBoundary = YES;
    [self.anim addBehavior:self.collision];
    
    UIDynamicItemBehavior *settings = [[UIDynamicItemBehavior alloc] initWithItems:@[self.part]];
    settings.density = 100.0f;
    settings.friction = 100.0f;
    [self.anim addBehavior:settings];
    
    if(self.times == 20){ [self.timer invalidate]; self.timer = nil;[self toMainMenu];}

}

-(void) toMainMenu{
    [self performSegueWithIdentifier:@"toMenu" sender:self];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"toMenu"]){
        ViewController *view = [segue destinationViewController ];
        view.productArray = self.productArray;
    }
}

@end
