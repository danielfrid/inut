//
//  GraphicViewController.h
//  iNut
//
//  Created by IT-Högskolan on 20/02/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MechanicsViewController : NSObject 
+(void) setButtonGraphics:(UIButton *)btn;
+(NSMutableArray *) searchMatApi;
+(void)loadApiDetails:(NSString *)searchText forCell:(UITableViewCell *)cell andTableView:(UITableView *)view;
+(void)setAddButtonGraphics:(UIButton *)btn;
+(void)setRemButtonGraphics:(UIButton *)btn;
+(void)setCompareBoxGraphics:(UIView *)box;
+(void)setHollowGraphics:(UIView *)box;
+(void)setFavButtGraphics:(UIView *)box;
+(void)saveToFile:(NSMutableArray *)userArray;
+(NSMutableArray *)loadUserData;
@end
