//
//  ProduktTableViewCell.m
//  iNut
//
//  Created by IT-Högskolan on 22/02/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "ProduktTableViewCell.h"

@implementation ProduktTableViewCell

- (void)awakeFromNib {
    // Initialization code
    _uiLabels = [NSArray arrayWithObjects: _proteinLabel, _fettLabel, _energyLabel, _waterLabel, _watLabel, _nrjLabel, _proLabel, _fatLabel, _addButton, nil];
  
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
