//
//  CombatUIVIewController.h
//  iNut
//
//  Created by IT-Högskolan on 19/03/15.
//  Copyright (c) 2015 IT-H&#246;gskolan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GKBarGraph.h>

@interface CombatUIVIewController : UIViewController <GKBarGraphDataSource>

@property (nonatomic) NSMutableArray *foodToCompare;
@end
