//
//  DetailViewController.h
//  iNut
//
//  Created by IT-Högskolan on 21/02/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LDFProduktInformation.h"
#import "MechanicsViewController.h"

@interface DetailViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (nonatomic) LDFProduktInformation *prod;
@property (nonatomic) NSMutableArray *userArray;

@end
