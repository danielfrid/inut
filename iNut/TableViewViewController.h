//
//  TableViewViewController.h
//  iNut
//
//  Created by IT-Högskolan on 21/02/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewViewController : UITableView

@end
