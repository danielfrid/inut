//
//  CombatUIVIewController.m
//  iNut
//
//  Created by IT-Högskolan on 19/03/15.
//  Copyright (c) 2015 IT-H&#246;gskolan. All rights reserved.
//

#import "CombatUIVIewController.h"
#import "LDFProduktInformation.h"

@interface CombatUIVIewController ()
@property (weak, nonatomic) IBOutlet GKBarGraph *graph;
@property (weak, nonatomic) IBOutlet UILabel *foodOne;
@property (weak, nonatomic) IBOutlet UILabel *foodtwo;
@property (weak, nonatomic) IBOutlet UIView *levelOne;
@property (weak, nonatomic) IBOutlet UIView *leveltwo;
@property (nonatomic) NSMutableArray *values;

@end
@implementation CombatUIVIewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.graph.dataSource = self;
    [self.graph draw];
    LDFProduktInformation *food = self.foodToCompare[0];
    self.foodOne.text = [NSString stringWithFormat:@"1. %@", food.produktName];
    food = self.foodToCompare[1];
    self.foodtwo.text = [NSString stringWithFormat:@"2. %@", food.produktName ];

}
-(void)viewDidLayoutSubviews{
    [self levelOfNutri];
}
-(void)levelOfNutri{
    double valueOne;
    double valueTwo;
    int counter = 0;
    
    for(NSNumber *num in self.values){
        counter++ ;
        int x = [num intValue];
        if(counter%2==0){
            valueOne += x;
        } else{
            valueTwo += x;
        }
    }
    valueOne = valueOne/100;
    valueTwo = valueTwo/100;
    CGRect frame = self.levelOne.frame;
    [self.levelOne setFrame:CGRectMake(frame.origin.x, frame.origin.y, valueOne, frame.size.height)];
    frame = self.leveltwo.frame;
    [self.leveltwo setFrame:CGRectMake(frame.origin.x, frame.origin.y, valueTwo, frame.size.height)];
    
  
}

-(NSInteger)numberOfBars {
    return 8;
}


- (NSNumber *)valueForBarAtIndex:(NSInteger)index {
    
    LDFProduktInformation *prodOne = self.foodToCompare[0];
    LDFProduktInformation *prodTwo = self.foodToCompare[1];

    self.values = [[NSMutableArray alloc] initWithObjects:
                   [NSNumber numberWithInteger:[prodOne.fatIndex integerValue]],
                   [NSNumber numberWithInteger:[prodTwo.fatIndex integerValue]],
                   [NSNumber numberWithInteger:[prodOne.proteinIndex integerValue]],
                   [NSNumber numberWithInteger:[prodTwo.proteinIndex integerValue]],
                   [NSNumber numberWithInteger:[prodOne.waterIndex integerValue]],
                   [NSNumber numberWithInteger:[prodTwo.waterIndex integerValue]],
                   [NSNumber numberWithInteger:[prodOne.energyIndex integerValue]],
                   [NSNumber numberWithInteger:[prodTwo.energyIndex integerValue]],
                   nil];

    return [self.values objectAtIndex:index];
}

- (UIColor *)colorForBarAtIndex:(NSInteger)index {

    NSArray *colors = [NSArray arrayWithObjects:
                                 (id)[UIColor colorWithRed:178.0f / 255.0f green:0.0f / 255.0f blue:0.0f / 255.0f alpha:1.0f],
                                 (id)[UIColor colorWithRed:178.0f / 255.0f green:0.0f / 255.0f blue:0.0f / 255.0f alpha:1.0f],
                                (id)[UIColor colorWithRed:0.0f / 255.0f green:51.0f / 255.0f blue:204.0f / 255.0f alpha:1.0f],
                                (id)[UIColor colorWithRed:0.0f / 255.0f green:51.0f / 255.0f blue:204.0f / 255.0f alpha:1.0f],
                                (id)[UIColor colorWithRed:0.0f / 255.0f green:153.0f / 255.0f blue:51.0f / 255.0f alpha:1.0f],
                                (id)[UIColor colorWithRed:0.0f / 255.0f green:153.0f / 255.0f blue:51.0f / 255.0f alpha:1.0f],
                                (id)[UIColor colorWithRed:255.0f / 255.0f green:153.0f / 255.0f blue:0.0f / 255.0f alpha:1.0f],
                                (id)[UIColor colorWithRed:255.0f / 255.0f green:153.0f / 255.0f blue:0.0f / 255.0f alpha:1.0f],
                                nil];
    return [colors objectAtIndex:index];
}

- (UIColor *)colorForBarBackgroundAtIndex:(NSInteger)index {
    return [UIColor colorWithRed:204.0f / 255.0f green:204.0f / 255.0f blue:204.0f / 255.0f alpha:1.0f];
}

- (CFTimeInterval)animationDurationForBarAtIndex:(NSInteger)index {
    return 0.5f;
}

- (NSString *)titleForBarAtIndex:(NSInteger)index {
    if (index%2==0) {
        return @"1";
    } else {
        return @"2";
    }
}

@end
