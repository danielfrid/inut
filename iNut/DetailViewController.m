//
//  DetailViewController.m
//  iNut
//
//  Created by IT-Högskolan on 21/02/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *fatLabel;
@property (weak, nonatomic) IBOutlet UILabel *waterLabel;
@property (weak, nonatomic) IBOutlet UILabel *nrjLabel;
@property (weak, nonatomic) IBOutlet UILabel *protLabel;
@property (weak, nonatomic) IBOutlet UILabel *foodName;
@property (weak, nonatomic) IBOutlet UIImageView *photo;


@end

@implementation DetailViewController

- (NSString*)cachePath {
    NSArray *dirs = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentDir = dirs[0];
    
    
    //Ask wtf this turnes iot a long32 if not stringWithFormat
    NSString *imageName = [NSString stringWithFormat:@"%@", self.prod.produktIndexNr];
    NSString *completePath = [documentDir stringByAppendingPathComponent:imageName];
    NSLog(@"cachePath: %@", completePath);
    return completePath;
}

- (void) saveImageToCache: (UIImage *)image{
    NSData *data = UIImagePNGRepresentation(self.photo.image);
    BOOL success = [data writeToFile:[self cachePath] atomically:YES];
    self.prod.imagePath = [self cachePath];
    [MechanicsViewController saveToFile:self.userArray];

    if(!success){
        NSLog(@"Failed save img to cahce!");
    }

}
- (IBAction)photoButt:(id)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:nil];
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
        [picker dismissViewControllerAnimated:YES completion:nil];
    
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    self.photo.image = image;
    [self saveImageToCache:image];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.userArray = [MechanicsViewController loadUserData];
    self.fatLabel.text = self.prod.fatIndex;
    self.waterLabel.text = self.prod.waterIndex;
    self.nrjLabel.text = self.prod.energyIndex;
    self.protLabel.text = self.prod.proteinIndex;
    self.foodName.text = self.prod.produktName;
    
    UIImage *image = [UIImage imageWithContentsOfFile:self.cachePath];
    if(image){
        self.photo.image = image;
    } else {
        NSLog(@"Failed to load img");
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
