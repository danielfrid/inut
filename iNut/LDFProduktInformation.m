//
//  LDFProduktInformation.m
//  iNut
//
//  Created by IT-Högskolan on 22/02/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "LDFProduktInformation.h"

@implementation LDFProduktInformation

- (id)initWithCoder:(NSCoder *)decoder{
    self = [super init];
    if(!self){return nil;}
    self.produktName = [decoder decodeObjectForKey:@"ProduktName"];
    self.produktIndexNr = [decoder decodeObjectForKey:@"ProduktIndexNr"];
    self.fatIndex = [decoder decodeObjectForKey:@"fatIndex"];
    self.waterIndex = [decoder decodeObjectForKey:@"waterIndex"];
    self.proteinIndex = [decoder decodeObjectForKey:@"proteinIndex"];
    self.energyIndex = [decoder decodeObjectForKey:@"energyIndex"];
    self.imagePath = [decoder decodeObjectForKey:@"imagePath"];
    self.isFav = [decoder decodeBoolForKey:@"isFav"];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder{
    [encoder encodeObject:self.produktName forKey:@"ProduktName"];
    [encoder encodeObject:self.produktIndexNr forKey:@"ProduktIndexNr"];
    [encoder encodeObject:self.fatIndex forKey:@"fatIndex"];
    [encoder encodeObject:self.waterIndex forKey:@"waterIndex"];
    [encoder encodeObject:self.proteinIndex forKey:@"proteinIndex"];
    [encoder encodeObject:self.energyIndex forKey:@"energyIndex"];
    [encoder encodeObject:self.imagePath forKey:@"imagePath"];
    [encoder encodeBool:self.isFav forKey:@"isFav"];

}


@end
