//
//  main.m
//  iNut
//
//  Created by IT-Högskolan on 18/02/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
