//
//  UserDataViewCell.h
//  iNut
//
//  Created by IT-Högskolan on 18/03/15.
//  Copyright (c) 2015 IT-H&#246;gskolan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserDataViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *foodImage;
@property (weak, nonatomic) IBOutlet UILabel *foodName;
@property (weak, nonatomic) IBOutlet UILabel *fatIndex;
@property (weak, nonatomic) IBOutlet UILabel *protIndex;
@property (weak, nonatomic) IBOutlet UILabel *waterIndex;
@property (weak, nonatomic) IBOutlet UILabel *nrjIndex;
@property (weak, nonatomic) IBOutlet UIButton *compareButton;
@property (weak, nonatomic) IBOutlet UIButton *removeButton;

@end
