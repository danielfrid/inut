//
//  MySettingsViewController.m
//  iNut
//
//  Created by IT-Högskolan on 20/02/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "UserDataViewController.h"
#import "MechanicsViewController.h"
#import "CombatUIVIewController.h"
#import "DetailViewController.h"
#import "UserDataViewCell.h"
#import "LDFProduktInformation.h"

@interface UserDataViewController ()
@property (weak, nonatomic) IBOutlet UIView *compareOne;
@property (weak, nonatomic) IBOutlet UIView *compareTwo;
@property (weak, nonatomic) IBOutlet UIView *compareThree;
@property (weak, nonatomic) IBOutlet UILabel *compareProdOne;
@property (weak, nonatomic) IBOutlet UILabel *compareProdTwo;
@property (weak, nonatomic) IBOutlet UILabel *compareProdThree;
@property (weak, nonatomic) IBOutlet UIButton *compareButton;
@property (weak, nonatomic) IBOutlet UIButton *clearCompButton;
@property (nonatomic) LDFProduktInformation *prod;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) int combatans;
@property (nonatomic) NSMutableArray *foodToCompare;
@end

@implementation UserDataViewController
- (IBAction)addToCompare:(id)sender {
    if(self.combatans < 3){
        
        CGPoint buttonPosition = [sender convertPoint:CGPointZero
                                               toView:self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
        self.prod = self.userArray[indexPath.row];
        self.combatans++;
        switch (self.combatans) {
            case 1:{
                self.compareProdOne.hidden = NO;
                self.compareProdOne.text = self.prod.produktName;
                self.compareOne.backgroundColor = [UIColor grayColor];
                self.foodToCompare = [[NSMutableArray alloc] initWithObjects:self.prod, nil];
                break;
            }
            case 2:{
                self.compareProdTwo.hidden = NO;
                self.compareProdTwo.text = self.prod.produktName;
                self.compareTwo.backgroundColor = [UIColor colorWithRed:20 / 255.0f green:160 / 255.0f blue:40 / 255.0f alpha:1.0f];
                self.compareOne.backgroundColor = [UIColor colorWithRed:20 / 255.0f green:160 / 255.0f blue:40 / 255.0f alpha:1.0f];
                [self.foodToCompare addObject:self.prod];
                [self.compareButton setEnabled:YES];
                [self.compareButton setTitleColor:[UIColor yellowColor] forState:UIControlStateNormal];
                break;
            }
            case 3:{
                self.compareProdThree.hidden = NO;
                self.compareProdThree.text = @"Not avail.";
                self.compareThree.backgroundColor = [UIColor colorWithRed:20 / 255.0f green:160 / 255.0f blue:40 / 255.0f alpha:1.0f];
                break;
            }
            default:
                break;
        }
    }
    
    self.prod.toCompare = YES;
}

- (IBAction)clearCompButton:(id)sender {
    [self resetCompare];
}

- (IBAction)removeObj:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero
                                           toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    LDFProduktInformation *removeProd = self.userArray[indexPath.row];
    removeProd.isFav = NO;
    [self.userArray removeObject:removeProd];
    [self resetCompare];
    [MechanicsViewController saveToFile:self.userArray];
    [self.tableView reloadData];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.compareButton setEnabled:NO];
    
    [MechanicsViewController setAddButtonGraphics:self.compareButton];
    [MechanicsViewController setAddButtonGraphics:self.clearCompButton];
    [self resetCompare];
}

-(void) resetCompare{
    self.compareProdOne.hidden = YES;
    self.compareProdTwo.hidden = YES;
    self.compareProdThree.hidden = YES;
    [MechanicsViewController setHollowGraphics:self.compareButton];
    [MechanicsViewController setHollowGraphics:self.compareOne];
    [MechanicsViewController setHollowGraphics:self.compareTwo];
    [MechanicsViewController setHollowGraphics:self.compareThree];
    self.combatans = 0;
    for (LDFProduktInformation *prod in self.userArray) {
        prod.toCompare = NO;
    }
    [self.foodToCompare removeAllObjects];
    [self.compareButton setEnabled:NO];
    [self.compareButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.userArray count];

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.prod = self.userArray[indexPath.row];
    [self performSegueWithIdentifier:@"toDetail" sender:self];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UserDataViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"userCell" forIndexPath:indexPath];
    self.prod = [self.userArray objectAtIndex:indexPath.row];
    
    UIImage *image = [UIImage imageWithContentsOfFile:self.cachePath];
    if(image){
        cell.foodImage.image = image;
    }
    
    [MechanicsViewController setCompareBoxGraphics:cell.compareButton];
    [MechanicsViewController setRemButtonGraphics:cell.removeButton];
 
    cell.fatIndex.text = self.prod.fatIndex;
    cell.protIndex.text = self.prod.proteinIndex;
    cell.waterIndex.text = self.prod.waterIndex;
    cell.nrjIndex.text = self.prod.energyIndex;
    cell.foodName.text = self.prod.produktName;
 
        return cell;
}

- (NSString*)cachePath {
    NSArray *dirs = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDir = dirs[0];
    
    
    //Ask wtf this turnes iot a long32 if not stringWithFormat
    NSString *imageName = [NSString stringWithFormat:@"%@", self.prod.produktIndexNr];
    NSString *completePath = [documentDir stringByAppendingPathComponent:imageName];
    return completePath;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"toDetail"]){
        DetailViewController *view = [segue destinationViewController];
        view.prod = self.prod;
       }
    if([segue.identifier isEqualToString:@"compare"]){
        CombatUIVIewController *view = [segue destinationViewController];
        view.foodToCompare = self.foodToCompare;
    }
}


@end
