//
//  AboutViewController.m
//  iNut
//
//  Created by IT-Högskolan on 20/02/15.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "AboutViewController.h"
#import "MechanicsViewController.h"

@interface AboutViewController ()
@property (weak, nonatomic) IBOutlet UIButton *button;

@end

@implementation AboutViewController
- (IBAction)okeyButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];

}
- (void)viewDidLayoutSubviews{
     [MechanicsViewController setButtonGraphics:self.button];
}
- (void)viewDidLoad {
    [super viewDidLoad];
   
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
